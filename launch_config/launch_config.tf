resource "aws_launch_template" "launch_template" {
  name                   = "terraform-launch-template"
  image_id               = var.ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = var.security_groups
  user_data              = filebase64("./bootstrap.sh")
  lifecycle {
    create_before_destroy = true
  }
}

data "aws_subnet" "subnet" {
  for_each = toset(var.subnets)
  id       = each.key
}

resource "aws_autoscaling_group" "auto_scaling_group" {
  name                 = "terraform-auto-scaling-group"
  availability_zones   = [for s in data.aws_subnet.subnet : s.availability_zone]
  desired_capacity     = 2
  max_size             = 4
  min_size             = 2
  termination_policies = ["OldestInstance"]
  launch_template {
    id      = aws_launch_template.launch_template.id
    version = "$Latest"
  }
  lifecycle {
    create_before_destroy = true
  }
  target_group_arns = [var.target_group_arn]
}

locals {
  scale_up_start_time   = timeadd(timestamp(), "1m")
  scale_up_end_time     = timeadd(timestamp(), "2m")
  scale_down_start_time = timeadd(timestamp(), "5m")
  scale_down_end_time   = timeadd(timestamp(), "8m")
}
resource "aws_autoscaling_schedule" "scale_up" {
  scheduled_action_name  = "scale up to 4"
  min_size               = 2
  max_size               = 4
  desired_capacity       = 4
  start_time             = local.scale_up_start_time
  end_time               = local.scale_up_end_time
  autoscaling_group_name = aws_autoscaling_group.auto_scaling_group.name
}

resource "aws_autoscaling_schedule" "scale_down" {
  scheduled_action_name  = "scale down to 2"
  min_size               = 2
  max_size               = 4
  desired_capacity       = 2
  start_time             = local.scale_down_end_time
  end_time               = local.scale_down_end_time
  autoscaling_group_name = aws_autoscaling_group.auto_scaling_group.name
}