output "vpc_info" {
  value = {
    vpc_id   = data.aws_vpc.vpc.id
    subnets  = data.aws_subnet_ids.subnets.ids
    ssh_http = data.aws_security_group.ssh_http.id
  }
}