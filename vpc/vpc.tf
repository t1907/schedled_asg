data "aws_vpc" "vpc" {
  default = true
}

data "aws_subnet_ids" "subnets" {
  vpc_id = data.aws_vpc.vpc.id
}

data "aws_security_group" "ssh_http" {
  name   = "ssh-http-sg"
  vpc_id = data.aws_vpc.vpc.id
}
