provider "aws" {
  region = "us-east-1"
}


module "vpc" {
  source  = "./vpc"
  segment = "test"
  region  = "us-east-1"
}

module "alb" {
  source = "./alb"
}

module "asg" {
  source           = "./launch_config"
  security_groups  = [module.vpc.vpc_info["ssh_http"]]
  subnets          = module.vpc.vpc_info["subnets"]
  load_balancer    = module.alb.load_balancer_name
  target_group_arn = module.alb.target_group_arn
  depends_on       = [module.alb]
  ami              = var.ami
}

variable "ami" {
  default = "ami-087c17d1fe0178315"
}