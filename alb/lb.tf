data "aws_lb" "alb" {
  name = "test-lb-tf"
}

data "aws_lb_target_group" "target_group" {
  name = "tf-example-lb-tg"
}
