output "load_balancer_name" {
  value = data.aws_lb.alb.name
}

output "target_group_arn" {
  value = data.aws_lb_target_group.target_group.arn
}